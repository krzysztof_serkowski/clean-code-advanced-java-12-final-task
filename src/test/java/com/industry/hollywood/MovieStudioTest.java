package com.industry.hollywood;

import com.industry.hollywood.MovieStudio.MovieDefinition;
import thirdparty.service.InsufficientBudgetException;
import thirdparty.Genre;
import com.industry.hollywood.movie.Movie;
import com.industry.hollywood.staff.Actor;
import com.industry.hollywood.staff.CameraMan;
import com.industry.hollywood.staff.team.StudioStaff;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MovieStudioTest {

    MovieStudio movieStudio;
    String recruiterForTest = "Andrew Carnegie";
    String accountantForTest = "William Welch Deloitte";

    @Before
    public void setUp() {
        movieStudio = new MovieStudio();
        movieStudio.printMovieArchiveStatistics();
    }

    @Test
    public void CreateMovie_Titanic_WithValidMovieDefinition_ReturnsTrue_OnSuccessfulCompletion() throws InsufficientBudgetException {
        int PRODUCTION_SCHEDULE = 160;
        StudioStaff staff = new StudioStaff(
                new Actor[]{
                        new Actor("Leo DiCaprio", true), new Actor("Kate Winslet", true),
                        new Actor("Billy Zane", false), new Actor("Kathy Bates", false),
                        new Actor("Frances Fisher", false), new Actor("Bernard Hill", false),
                        new Actor("Jonathan Hyde", false), new Actor("Danny Nucci", false),
                        new Actor("David Warner", false), new Actor("Bill Paxton", false)
                },
                new CameraMan[]{
                        new CameraMan("Guy Norman Bee"),
                        new CameraMan("Marcis Cole"),
                        new CameraMan("Tony Guerin")
                }
        );
        Long budget = 160000000L;
        MovieDefinition titanicMovie = new MovieDefinition(budget, "Titanic", Genre.DRAMA,
                staff, PRODUCTION_SCHEDULE);
        Movie movie = movieStudio.createMovie(recruiterForTest, accountantForTest, titanicMovie);
        assertTrue(movie.isFinished());
    }

    @Test
    public void CreateMovie_StarWars_WithValidMovieDefinition_ReturnsTrue_OnSuccessfulCompletion() throws InsufficientBudgetException {
        int PRODUCTION_SCHEDULE = 90;
        StudioStaff staff = new StudioStaff(
                new Actor[]{
                        new Actor("Mark Hamill", true), new Actor("Harrison Ford", true),
                        new Actor("Carrie Fischer", false), new Actor("Billy Dee Williams", false),
                        new Actor("Anthony Daniels", false), new Actor("David Prowse", false),
                        new Actor("Peter Mayhew", false)
                },
                new CameraMan[]{
                        new CameraMan("John Campbell"),
                        new CameraMan("Bill Neil")
                }
        );
        Long budget = 60000000L;
        MovieDefinition starWars3Movie = new MovieDefinition(budget, "Star Wars: Episode VI – Return of the Jedi",
                Genre.SCIFI, staff, PRODUCTION_SCHEDULE);
        Movie movie = movieStudio.createMovie(recruiterForTest, accountantForTest, starWars3Movie);
        assertTrue(movie.isFinished());
    }

    @Test
    public void CreateMovie_ShouldCreateEmptyMovie_WithEmptyMovieDefinition_ReturnsTrue() throws InsufficientBudgetException {
        int PRODUCTION_SCHEDULE = 1;
        StudioStaff staff = new StudioStaff(
                new Actor[]{},
                new CameraMan[]{}
        );
        Long budget = 1L;
        MovieDefinition emptyMovie = new MovieDefinition(budget, "Noname", Genre.COMEDY,
                staff, PRODUCTION_SCHEDULE);
        Movie movie = movieStudio.createMovie(recruiterForTest, accountantForTest, emptyMovie);
        assertTrue(movie.isFinished());
    }

    @Test
    public void CreateMovie_WhenBudgetExceeds_ReturnsFalse_OnUnsuccessfulCompletion() throws InsufficientBudgetException {
        int PRODUCTION_SCHEDULE = 200;
        StudioStaff staff = new StudioStaff(
                new Actor[]{
                        new Actor("Taylor Kitsch", false),
                        new Actor("Lynn Collins", false),
                        new Actor("Samantha Morton", false),
                        new Actor("Mark Strong", true),
                        new Actor("Ciarán Hinds", false),
                        new Actor("Dominic West", false),
                        new Actor("James Purefoy", false),
                        new Actor("Willem Dafoe", true)
                },
                new CameraMan[]{
                        new CameraMan("Carver Christians"),
                        new CameraMan("Scott Bourke"),
                        new CameraMan("Quentin Herriot"),
                        new CameraMan("Brandon Wyman")
                }
        );
        Long budget = 100000000L;
        MovieDefinition johnCarterMovie = new MovieDefinition(budget, "John Carter", Genre.FANTASY,
                staff, PRODUCTION_SCHEDULE);
        Movie movie = movieStudio.createMovie(recruiterForTest, accountantForTest, johnCarterMovie);
        assertFalse(movie.isFinished());
    }

    @Test(expected = InsufficientBudgetException.class)
    public void CreateMovie_WhenInsufficientBudget_Throws_InsufficientBudgetException() throws InsufficientBudgetException {
        int PRODUCTION_SCHEDULE = 250;
        StudioStaff staff = new StudioStaff(
                new Actor[]{
                        new Actor("Channing Tatum", true),
                        new Actor("Taylor Kitsch", true),
                        new Actor("Keanu Reeves", true),
                        new Actor("Josh Holloway", true),
                        new Actor("Léa Seydoux", true),
                        new Actor("Hugh Jackman", true),
                        new Actor("Rebecca Ferguson", false),
                        new Actor("Abbey Leee", true)
                },
                new CameraMan[]{
                        new CameraMan("Carver Christians"),
                        new CameraMan("Scott Bourke"),
                        new CameraMan("Quentin Herriot"),
                        new CameraMan("Brandon Wyman")
                }
        );
        Long budget = 100000000L;
        MovieDefinition gambitMovie = new MovieDefinition(budget, "Gambit", Genre.FANTASY,
                staff, PRODUCTION_SCHEDULE);
        movieStudio.createMovie(recruiterForTest, accountantForTest, gambitMovie);
    }

}