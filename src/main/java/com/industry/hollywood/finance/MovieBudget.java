package com.industry.hollywood.finance;

public class MovieBudget {

    private Long budgetMoney;

    public Long getBudgetMoney() {
        return budgetMoney;
    }

    public void setBudgetMoney(Long budgetMoney) {
        this.budgetMoney = budgetMoney;
    }

    public MovieBudget(Long budgetMoney) {
        this.budgetMoney = budgetMoney;
    }
}
