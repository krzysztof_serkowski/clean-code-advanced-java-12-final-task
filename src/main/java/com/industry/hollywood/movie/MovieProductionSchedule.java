package com.industry.hollywood.movie;

public class MovieProductionSchedule {

    private int daysInProduction;

    public int getDaysSpentOnProduction() {
        return daysInProduction;
    }

    public void setDaysInProduction(int daysInProduction) {
        this.daysInProduction = daysInProduction;
    }

    public MovieProductionSchedule(int daysInProduction) {
        this.daysInProduction = daysInProduction;
    }
}
