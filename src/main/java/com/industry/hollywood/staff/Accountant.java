package com.industry.hollywood.staff;

import com.industry.hollywood.staff.profile.EmployeeFunctionality;
import thirdparty.service.BudgetIsOverException;
import thirdparty.service.FinancialService;
import thirdparty.staff.StudioEmployee;

import static thirdparty.Salaries.ACCOUNTANT;

public class Accountant extends StudioEmployee implements EmployeeFunctionality {

    public Accountant(String name) {
        super(name, ACCOUNTANT.proposedSalary);
    }

    @Override
    public void pay(StudioEmployee person, FinancialService financialService) throws BudgetIsOverException {
        Long salary = person.getSalary();
        person.paySalary(salary);
        if ((financialService.getBudget() - salary) < 0) {
            financialService.initBudget(0L);
            throw new BudgetIsOverException();
        }
        financialService.decreaseBudget(salary);
    }

    @Override
    public boolean act() {
        return false;
    }

    @Override
    public boolean shoot() {
        return false;
    }

    @Override
    public StudioEmployee hire(String name, String personType) {
        return null;
    }
}
