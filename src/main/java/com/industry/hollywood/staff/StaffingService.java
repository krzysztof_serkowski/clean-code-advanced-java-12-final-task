package com.industry.hollywood.staff;

import com.industry.hollywood.staff.team.StudioStaff;
import thirdparty.staff.StudioEmployee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StaffingService {

    private final List<StudioEmployee> staff;

    public StaffingService() {
        staff = new ArrayList<>();
    }

    public void hireNewStaff(StudioEmployee... persons) {
        staff.clear();
        Collections.addAll(staff, persons);
    }

    public void hireNewStaff(StudioStaff movieDefinition) {
        staff.addAll(movieDefinition.getActorsCollection());
        staff.addAll(movieDefinition.getCameramanCollection());
    }

    public List<StudioEmployee> getStaff() {
        return staff;
    }

}
