package thirdparty.staff;

public abstract class StudioEmployee {

    private final String name;
    protected Long salary;
    protected Long earnedMoney; // total amount of earned money

    public StudioEmployee(String name, Long initialSalary) {
        this.name = name;
        this.salary = initialSalary;
        this.earnedMoney = 0L;
    }

    public Long getEarnedMoney() {
        return earnedMoney;
    }

    public String getName() {
        return name;
    }

    public Long getSalary() {
        return salary;
    }

    public void paySalary(Long paidSum) {
        this.earnedMoney += paidSum;
    }

}
