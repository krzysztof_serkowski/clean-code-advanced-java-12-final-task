package thirdparty.service;

import thirdparty.staff.StudioEmployee;

import java.util.List;

public interface FinancialService {

    void initBudget(Long initialSum);

    void decreaseBudget(Long paidSum) throws BudgetIsOverException;

    void paySalary(List<StudioEmployee> employees) throws BudgetIsOverException;

    Long getBudget();
}
